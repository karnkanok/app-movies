import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'action',
    loadChildren: () => import('./details-movies/action/action.module').then( m => m.ActionPageModule)
  },
  {
    path: 'crime',
    loadChildren: () => import('./details-movies/crime/crime.module').then( m => m.CrimePageModule)
  },
  {
    path: 'horror',
    loadChildren: () => import('./details-movies/horror/horror.module').then( m => m.HorrorPageModule)
  },
  {
    path: 'drama',
    loadChildren: () => import('./details-movies/drama/drama.module').then( m => m.DramaPageModule)
  },
  {
    path: 'animation',
    loadChildren: () => import('./details-movies/animation/animation.module').then( m => m.AnimationPageModule)
  },
  {
    path: 'all-movies',
    loadChildren: () => import('./details-movies/all-movies/all-movies.module').then( m => m.AllMoviesPageModule)
  },
  {
    path: 'promotion',
    loadChildren: () => import('./details-movies/promotion/promotion.module').then( m => m.PromotionPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
