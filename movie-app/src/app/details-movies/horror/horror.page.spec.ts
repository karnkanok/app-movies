import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HorrorPage } from './horror.page';

describe('HorrorPage', () => {
  let component: HorrorPage;
  let fixture: ComponentFixture<HorrorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorrorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HorrorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
