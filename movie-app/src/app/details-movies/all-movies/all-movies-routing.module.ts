import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllMoviesPage } from './all-movies.page';

const routes: Routes = [
  {
    path: '',
    component: AllMoviesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllMoviesPageRoutingModule {}
