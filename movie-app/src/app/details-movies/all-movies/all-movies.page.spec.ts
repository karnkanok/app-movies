import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllMoviesPage } from './all-movies.page';

describe('AllMoviesPage', () => {
  let component: AllMoviesPage;
  let fixture: ComponentFixture<AllMoviesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMoviesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllMoviesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
