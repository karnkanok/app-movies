import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllMoviesPageRoutingModule } from './all-movies-routing.module';

import { AllMoviesPage } from './all-movies.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllMoviesPageRoutingModule
  ],
  declarations: [AllMoviesPage]
})
export class AllMoviesPageModule {}
