import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrimePage } from './crime.page';

const routes: Routes = [
  {
    path: '',
    component: CrimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrimePageRoutingModule {}
