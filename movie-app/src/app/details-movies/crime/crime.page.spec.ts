import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrimePage } from './crime.page';

describe('CrimePage', () => {
  let component: CrimePage;
  let fixture: ComponentFixture<CrimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
