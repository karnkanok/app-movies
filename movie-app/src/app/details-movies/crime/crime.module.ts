import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrimePageRoutingModule } from './crime-routing.module';

import { CrimePage } from './crime.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrimePageRoutingModule
  ],
  declarations: [CrimePage]
})
export class CrimePageModule {}
